require('bitbucket/feature/files/file-handlers').register({
    weight: 90,
    handle: function () {
        return require('stiltsoft/ipython-viewer/file-handlers').handle.apply(this, arguments);
    }
});