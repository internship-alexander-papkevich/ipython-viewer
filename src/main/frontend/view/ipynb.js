import $ from 'jquery';
import ipynbRenderer from '../ipynb-renderer';

const ipynbView = ($container, source) => {
    const $el = $('<div class="markup-content markup ipynb-content"/>');
    $container.append($el);

    ipynbRenderer($el, source);

    return {destroy: () => $el.remove(), extraClasses: 'ipynb-file'};
};

export default ipynbView;