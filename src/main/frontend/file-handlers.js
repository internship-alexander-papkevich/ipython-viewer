import ipynbView from './view/ipynb';
import {loadSource} from "./util";

const handle = ({ fileChange, contentMode, $container }) => {
    if (contentMode === 'source') {
        const ext = fileChange.path.extension.toLowerCase();

        if (ext === 'ipynb') {
            return loadSource(fileChange).then(source => ipynbView($container, source));
        }
    }
    return false;
}

export {handle};