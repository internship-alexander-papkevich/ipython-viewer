import {ajax, rest} from "bitbucket/util/server";
import nav from "bitbucket/util/navbuilder";
import $ from "jquery";

const
    THRESHOLD = 64 * 1024 * 1024, // 64 Mb
    lfsMatchers = {
        version: /version https:\/\/git-lfs\.github\.com\/spec\/v(\d)/,
        oid: /oid sha256:([a-fA-F0-9]{64})/,
        size: /size ([0-9]*)/,
    },
    lfsFields = Object.keys(lfsMatchers),
    extractLfsMetaData = payload => {
        const
            lines = payload.split('\n').filter(line => !!line),
            lfsMetaData = {},
            lineToMetaData = (line, index) => {
                const
                    field = lfsFields[index],
                    matches = line.match(lfsMatchers[field]);
                return matches ? (lfsMetaData[field] = matches[1]) : false;
            };

        return lines.length === 3 && lines.every(lineToMetaData) && lfsMetaData;
    };

const handleLfs = (fileChange, transformer, previousUrl) => payload => {
    if (typeof payload === 'string') {
        const lfsMetaData = extractLfsMetaData(payload);
        if (lfsMetaData) {

            if (Number(lfsMetaData.size) > THRESHOLD) {
                return $.Deferred().reject(false);
            }

            return rest({
                url: nav.currentRepo().clone('git').addPathComponents('info', 'lfs', 'objects', 'batch').build(),
                type: 'POST',
                dataType: 'json',
                accepts: {json: 'application/vnd.git-lfs+json; charset=UTF-8'},
                contentType: 'application/vnd.git-lfs+json; charset=UTF-8',
                data: {operation: 'download', objects: [lfsMetaData]},
                statusCode: {'*': false},
            }).then(response => {
                const name = fileChange.path.name,
                    lfsUrl = nav.parse(response.objects[0].actions.download.href);

                lfsUrl.addQueryParam('response-content-disposition', 'attachment; filename="' + name + "\"; filename*=utf-8''" + encodeURIComponent(name));

                $('.raw-view-link').attr('href', lfsUrl.toString());

                return transformer(response);
            });
        }
        return previousUrl || payload;
    }
    return previousUrl || payload;
};

const makeUrl = ({path, commitRange}) => nav.currentRepo().browse().path(path.components).at(commitRange.untilRevision).raw().build();

const loadSource = fileChange => ajax({url: makeUrl(fileChange)})
    .then(handleLfs(fileChange, response => ajax({url: response.objects[0].actions.download.href})));

const buildUrl = fileChange => {
    const url = makeUrl(fileChange);
    return ajax({url}).then(handleLfs(fileChange, response => response.objects[0].actions.download.href, url));
};

export {loadSource, buildUrl};