
const ipynbRenderer = ($container, source) => {
    const ipynb = nb.parse(JSON.parse(source));
    const notebook = ipynb.render();
    $container.html(notebook);
    MathJax.typeset();
    console.log(MathJax);
    Prism.highlightAll();
}

export default ipynbRenderer;