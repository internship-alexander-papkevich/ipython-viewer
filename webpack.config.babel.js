import path from "path";

const resolve = dir => path.resolve(__dirname, dir);

const
    server = {
        entry: {
            'file-handlers': './src/main/frontend/file-handlers.js',
        },
        output: {
            filename: 'js/gen/[name].js',
            path: resolve('target/classes'),
            libraryTarget: 'amd',
            library: 'stiltsoft/ipython-viewer/[name]'
        },
        externals: [
            'jquery',
            'bitbucket/util/navbuilder',
            'bitbucket/util/server',
            'stiltsoft/file-viewer/aui'
        ],
        devtool: 'source-map',
        optimization: {
            minimize: false
        },
        mode: 'development'
    },
    shared = {
        module: {
            rules: [
                {test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'}
            ]
        }
    };

export default [server].map(conf => Object.assign({}, shared, conf));